# Projecttemplate API Apitemplate

The description about this API should be here.
This project was first generated using https://start.spring.io/

-----

### Requirements

- Java 8
- Docker
- Lombok Plugin
- Enable Annotation Processing in Intellij Settings
- GCP - Google Cloud SDK with the additional package of Cloud Datastore Emulator. Access: https://cloud.google.com/sdk/docs/?hl=pt-br

-----

### Building and Running: Gradle

Database container should be running.

To build application jar:
```bash
./gradlew clean build
```

To run all tests
```bash
./gradlew test
```

To run locally, execute: 
```bash
./gradlew bootRun
```

To verify if the application is running, access:
```
http://localhost:8080/actuator/health
```

-----

### gcloud Commands

Start local datastore without persistence:
```bash
gcloud beta emulators datastore start --no-store-on-disk
```

Setup gcloud env variables: DATASTORE_EMULATOR_HOST and DATASTORE_PROJECT_ID automatically
```bash
$(gcloud beta emulators datastore env-init)
```


The credentials can be generated in Google Cloud Console:

- APIs and Services -> Credentials -> Create Credentials -> Service account key
    - Select New service account and type JSON
    - Setup a service account name
    - Role: Datastore -> Cloud Datastore User
    - Click on Create
    - setup the env variable:
        ```bash
        export GOOGLE_APPLICATION_CREDENTIALS=<location_of_key.json>
        ```
