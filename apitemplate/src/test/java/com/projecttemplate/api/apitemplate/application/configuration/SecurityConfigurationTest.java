package com.projecttemplate.api.apitemplate.application.configuration;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("SecurityConfiguration Class")
class SecurityConfigurationTest {

    @Nested
    @DisplayName("buildPasswordEncoder Method")
    class BuildPasswordEncoderMethod {

        @Test
        @DisplayName("Should returns an new instance of BCryptPasswordEncoder")
        void shouldReturnsAnInstanceOfBCryptPasswordEncoder() {
            SecurityConfiguration securityConfiguration = new SecurityConfiguration();

            PasswordEncoder passwordEncoder = securityConfiguration.buildPasswordEncoder();

            assertThat(passwordEncoder).isInstanceOf(BCryptPasswordEncoder.class);
        }

    }
}