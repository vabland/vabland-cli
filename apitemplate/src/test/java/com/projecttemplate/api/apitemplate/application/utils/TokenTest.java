package com.projecttemplate.api.apitemplate.application.utils;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
@DisplayName("Token Class")
class TokenTest {

    @Nested
    @DisplayName("create Method")
    class CreateMethod {

        @Test
        @DisplayName("should generates a new random token every time it is called")
        void alwaysGeneratesRandomTokens() {
            List<String> tokens = new ArrayList<>();
            for (int i = 0; i < 100; i++) {
                String token = Token.create();
                assertThat(tokens).doesNotContain(token);
                tokens.add(token);
            }
        }

        @Test
        @DisplayName("should generates a tokens with length of 100 characters")
        void generatedTokenHaveLengthOfOneHundred() {
            String token = Token.create();

            assertThat(token.length()).isEqualTo(100);
        }

    }
}