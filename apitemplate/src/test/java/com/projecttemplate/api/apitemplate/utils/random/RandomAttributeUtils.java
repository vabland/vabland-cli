package com.projecttemplate.api.apitemplate.utils.random;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.apache.commons.lang3.RandomStringUtils.*;

public class RandomAttributeUtils {

    public static String randomEmail() {
        return randomAlphanumeric(8) + "@" + randomAlphanumeric(5) + ".com";
    }

    public static String randomPassword() {
        return randomNumeric(1) +
                randomAlphabetic(1).toUpperCase() +
                randomAlphabetic(1).toLowerCase() +
                randomAlphanumeric(8);
    }

}
