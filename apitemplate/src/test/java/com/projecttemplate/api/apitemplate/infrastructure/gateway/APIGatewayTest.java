package com.projecttemplate.api.apitemplate.infrastructure.gateway;

import com.projecttemplate.api.apitemplate.domain.exception.APIGatewayConnectionException;
import com.projecttemplate.api.apitemplate.domain.exception.APIGatewayResponseException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@ExtendWith(MockitoExtension.class)
@DisplayName("APIGateway Class")
class APIGatewayTest {

    private static final String APPLICATION_JSON_CHARSET_UTF_8 = "application/json; charset=utf-8";
    private static final String HOST =  "https://host.com";

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private APIGateway apiGateway;

    @Nested
    @DisplayName("get Method")
    class GetMethod {

        @Test
        @DisplayName("should return an instance of APIGatewayBuilder")
        void callRestTemplateWithGetMethod() {
            APIGateway.APIGatewayRequestBuilder builder = apiGateway.get(HOST);

            assertThat(builder).isInstanceOf(APIGateway.APIGatewayRequestBuilder.class);
        }

    }

    @Nested
    @DisplayName("post Method")
    class PostMethod {

        @Test
        @DisplayName("should return an instance of APIGatewayBuilder")
        void callRestTemplateWithPostMethod() {
            APIGateway.APIGatewayRequestBuilder builder = apiGateway.post(HOST);

            assertThat(builder).isInstanceOf(APIGateway.APIGatewayRequestBuilder.class);
        }

    }

    @Nested
    @DisplayName("APIGatewayBuilder Inner Class")
    class APIGatewayBuilderInnerClass {

        private ArgumentCaptor<String> urlCaptor;
        private ArgumentCaptor<HttpMethod> httpMethodCaptor;
        private ArgumentCaptor<HttpEntity> httpEntityCaptor;
        private ResponseEntity<ResponseStub> expectedResponse;

        @BeforeEach
        void setUp() {
            urlCaptor = ArgumentCaptor.forClass(String.class);
            httpMethodCaptor = ArgumentCaptor.forClass(HttpMethod.class);
            httpEntityCaptor = ArgumentCaptor.forClass(HttpEntity.class);
            expectedResponse = ResponseEntity.ok(new ResponseStub());
        }

        @Nested
        @DisplayName("paths Method")
        class PathsMethod {

            @Test
            @DisplayName("should pass concatenate paths into URL")
            void shouldConcatenatePathsIntoURL() {
                when(restTemplate.exchange(urlCaptor.capture(),
                        httpMethodCaptor.capture(),
                        httpEntityCaptor.capture(),
                        eq(ResponseStub.class)))
                        .thenReturn(expectedResponse);

                apiGateway.get(HOST)
                        .paths("api", "path")
                        .execute(ResponseStub.class);

                assertThat(urlCaptor.getValue()).isEqualTo(HOST + "/api/path");
            }

        }

        @Nested
        @DisplayName("queryParam Method")
        class QueryParamMethod {

            @Test
            @DisplayName("should pass query params map into the URL")
            void shouldConcatenateQueryParamsIntoURL() {
                when(restTemplate.exchange(urlCaptor.capture(),
                        httpMethodCaptor.capture(),
                        httpEntityCaptor.capture(),
                        eq(ResponseStub.class)))
                        .thenReturn(expectedResponse);

                MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();
                queryParams.add("filter", "value");
                queryParams.add("sort", "order");

                apiGateway.get(HOST)
                        .queryParam(queryParams)
                        .execute(ResponseStub.class);

                assertThat(urlCaptor.getValue()).isEqualTo(HOST + "?filter=value&sort=order");
            }

        }

        @Nested
        @DisplayName("body Method")
        class bodyMethod {

            @Test
            @DisplayName("should pass query params map into the URL")
            void shouldConcatenateQueryParamsIntoURL() {
                when(restTemplate.exchange(urlCaptor.capture(),
                        httpMethodCaptor.capture(),
                        httpEntityCaptor.capture(),
                        eq(ResponseStub.class)))
                        .thenReturn(expectedResponse);

                RequestStub requestBody = new RequestStub();

                apiGateway.post(HOST)
                        .body(requestBody)
                        .execute(ResponseStub.class);

                assertThat(httpEntityCaptor.getValue().getBody()).isEqualTo(requestBody);
                assertThat(httpEntityCaptor.getValue().getBody()).isInstanceOf(RequestStub.class);
            }

        }

        @Nested
        @DisplayName("execute Method")
        class ExecuteMethod {

            @Test
            @DisplayName("should pass GET http method to restTemplate")
            void shouldPassGetHttpMethodToRestTemplate() {
                when(restTemplate.exchange(urlCaptor.capture(),
                        httpMethodCaptor.capture(),
                        httpEntityCaptor.capture(),
                        eq(ResponseStub.class)))
                        .thenReturn(expectedResponse);

                apiGateway.get(HOST).execute(ResponseStub.class);

                assertThat(httpMethodCaptor.getValue()).isEqualTo(GET);
            }

            @Test
            @DisplayName("should pass POST http method to restTemplate")
            void shouldPassPostHttpMethodToRestTemplate() {
                when(restTemplate.exchange(urlCaptor.capture(),
                        httpMethodCaptor.capture(),
                        httpEntityCaptor.capture(),
                        eq(ResponseStub.class)))
                        .thenReturn(expectedResponse);

                apiGateway.post(HOST).execute(ResponseStub.class);

                assertThat(httpMethodCaptor.getValue()).isEqualTo(POST);
            }

            @Test
            @DisplayName("should add content-type in the headers")
            void returnsProvidedResponseBodyFromGetRequest() {
                when(restTemplate.exchange(urlCaptor.capture(),
                        httpMethodCaptor.capture(),
                        httpEntityCaptor.capture(),
                        eq(ResponseStub.class)))
                        .thenReturn(expectedResponse);

                apiGateway.get(HOST).execute(ResponseStub.class);

                List<String> contentTypeHeader = httpEntityCaptor.getValue().getHeaders().get(HttpHeaders.CONTENT_TYPE);
                assertThat(contentTypeHeader).contains(APPLICATION_JSON_CHARSET_UTF_8);
            }

            @Test
            @DisplayName("should pass responseType to restTemplate and return the response with the same type")
            void shouldReturnResponseEntityWithSpecifiedResponseType() {
                when(restTemplate.exchange(urlCaptor.capture(),
                        httpMethodCaptor.capture(),
                        httpEntityCaptor.capture(),
                        eq(ResponseStub.class)))
                        .thenReturn(expectedResponse);

                ResponseEntity<ResponseStub> response = apiGateway.post(HOST).execute(ResponseStub.class);

                assertThat(response).isEqualTo(expectedResponse);
                assertThat(response.getBody()).isInstanceOf(ResponseStub.class);
            }

            @Test
            @DisplayName("should returns a ResponseEntity with the same status from RestTempalte when it is in the 4XX layer")
            void shouldReturnsSameStatusFromRestTemplateWhenIs4XX() {
                when(restTemplate.exchange(urlCaptor.capture(),
                        httpMethodCaptor.capture(),
                        httpEntityCaptor.capture(),
                        eq(ResponseStub.class)))
                        .thenThrow(HttpServerErrorException.create(NOT_FOUND, null, null, null, null));

                ResponseEntity<ResponseStub> response = apiGateway.post(HOST).execute(ResponseStub.class);

                assertThat(response.getStatusCode()).isEqualTo(NOT_FOUND);
            }

            @Test
            @DisplayName("should throws APIGatewayResponseException when RestTemplate returns a response with http status 5XX")
            void shouldThrowAPIGatewayResponseExceptionWhenRestTemplateReturns500() {
                when(restTemplate.exchange(urlCaptor.capture(),
                        httpMethodCaptor.capture(),
                        httpEntityCaptor.capture(),
                        eq(ResponseStub.class)))
                        .thenThrow(HttpServerErrorException.create(INTERNAL_SERVER_ERROR, null, null, null, null));

                assertThrows(APIGatewayResponseException.class,
                        () -> apiGateway.post(HOST).execute(ResponseStub.class),
                        "The service: " + HOST + " responded with https status: " + INTERNAL_SERVER_ERROR.toString());
            }

            @Test
            @DisplayName("should throws APIGatewayConnectionException when RestTemplate doesn't not find the destination")
            void shouldThrowAPIGatewayConnectionExceptionWhenRestTemplateDoesNotReachTheDestination() {
                when(restTemplate.exchange(urlCaptor.capture(),
                        httpMethodCaptor.capture(),
                        httpEntityCaptor.capture(),
                        eq(ResponseStub.class)))
                        .thenThrow(new ResourceAccessException(""));

                assertThrows(APIGatewayConnectionException.class,
                        () -> apiGateway.post(HOST).execute(ResponseStub.class),
                        "The service: " + HOST + " didn't respond");
            }

        }
    }

    private class ResponseStub {}

    private class RequestStub {}
}