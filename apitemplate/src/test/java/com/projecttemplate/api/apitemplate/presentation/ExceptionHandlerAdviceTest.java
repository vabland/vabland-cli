package com.projecttemplate.api.apitemplate.presentation;

import com.projecttemplate.api.apitemplate.domain.exception.APIGatewayConnectionException;
import com.projecttemplate.api.apitemplate.domain.exception.APIGatewayResponseException;
import com.projecttemplate.api.apitemplate.presentation.response.ApiErrorsResponse;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.time.LocalDateTime;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.*;

@ExtendWith(MockitoExtension.class)
@DisplayName("ExceptionHandlerAdvice Class")
class ExceptionHandlerAdviceTest {

    private static final String REQUEST_FAILED_CODE = "RequestFailed";
    private static final String GATEWAY_FAILED_CODE = "GatewayFailed";
    private static final String UNREACHABLE_GATEWAY_CODE = "UnreachableGateway";
    private static final String UNEXPECTED_EXCEPTION_CODE = "UnexpectedException";
    private static final String UNEXPECTED_EXCEPTION_MESSAGE = "Unexpected exception";

    @InjectMocks
    private ExceptionHandlerAdvice exceptionHandlerAdvice;

    @Nested
    @DisplayName("validationErrorHandler Method")
    class ValidationErrorHandlerMethod {

        private MethodArgumentNotValidException mockMethodArgumentNotValidException(String fieldErrorName,
                                                                                    String fieldErrorCode,
                                                                                    String fieldErrorMessage,
                                                                                    String objectErrorCode,
                                                                                    String objectErrorMessage) {

            FieldError fieldError = mock(FieldError.class);
            when(fieldError.getField()).thenReturn(fieldErrorName);
            when(fieldError.getCode()).thenReturn(fieldErrorCode);
            when(fieldError.getDefaultMessage()).thenReturn(fieldErrorMessage);

            String[] objectErrorCodes = {"objectError.error", objectErrorCode};
            ObjectError objectError = mock(FieldError.class);
            when(objectError.getCodes()).thenReturn(objectErrorCodes);
            when(objectError.getDefaultMessage()).thenReturn(objectErrorMessage);

            BindingResult bindingResult = mock(BindingResult.class);
            when(bindingResult.getFieldErrors()).thenReturn(singletonList(fieldError));
            when(bindingResult.getGlobalErrors()).thenReturn(singletonList(objectError));

            MethodArgumentNotValidException exception = mock(MethodArgumentNotValidException.class);
            when(exception.getBindingResult()).thenReturn(bindingResult);

            return exception;
        }

        @Test
        @DisplayName("should returns a BAD REQUEST response with body filled with APIErrorsResponse instance built from MethodArgumentNotValidException exception")
        void returnsCustomResponseWhenMethodArgumentNotValidExceptionIsThrown() {
            String fieldErrorName = "fieldError";
            String fieldErrorCode = "fieldErrorCode";
            String fieldErrorMessage = "FieldError message";
            String objectErrorCode = "objectError";
            String objectErrorMessage = "ObjectError message";

            MethodArgumentNotValidException exception = mockMethodArgumentNotValidException(fieldErrorName,
                    fieldErrorCode,
                    fieldErrorMessage,
                    objectErrorCode,
                    objectErrorMessage);

            ResponseEntity<ApiErrorsResponse> response = exceptionHandlerAdvice.validationErrorHandler(exception);

            assertAll("should return an instance of ResponseEntity with expected content",
                    () -> assertThat(response.getStatusCode()).isEqualTo(BAD_REQUEST),
                    () -> assertThat(response.getBody().getStatus()).isEqualTo(BAD_REQUEST.name()),
                    () -> assertThat(response.getBody().getCode()).isEqualTo(BAD_REQUEST.value()),
                    () -> assertThat(response.getBody().getTimestamp()).isInstanceOf(LocalDateTime.class),
                    () -> assertThat(response.getBody().getErrors().get(0).getField()).isEqualTo(fieldErrorName),
                    () -> assertThat(response.getBody().getErrors().get(0).getCode()).isEqualTo(fieldErrorCode),
                    () -> assertThat(response.getBody().getErrors().get(0).getMessage()).isEqualTo(fieldErrorMessage),
                    () -> assertThat(response.getBody().getErrors().get(1).getCode()).isEqualTo(objectErrorCode),
                    () -> assertThat(response.getBody().getErrors().get(1).getMessage()).isEqualTo(objectErrorMessage)
            );
        }

    }

    @Nested
    @DisplayName("apiGatewayFailed Method")
    class APIGatewayFailedMethod {

        @Test
        @DisplayName("should returns a BAD GATEWAY response with body filled with APIErrorsResponse instance built from APIGatewayResponseException exception")
        void returnsCustomResponseWhenAPIGatewayResponseExceptionIsThrown() {
            APIGatewayResponseException exception = mock(APIGatewayResponseException.class);
            when(exception.getMessage()).thenReturn("APIGatewayResponseException Error Message");

            ResponseEntity<ApiErrorsResponse> response = exceptionHandlerAdvice.apiGatewayFailed(exception);

            assertAll("should return an instance of ResponseEntity with expected content",
                    () -> assertThat(response.getStatusCode()).isEqualTo(BAD_GATEWAY),
                    () -> assertThat(response.getBody().getStatus()).isEqualTo(BAD_GATEWAY.name()),
                    () -> assertThat(response.getBody().getCode()).isEqualTo(BAD_GATEWAY.value()),
                    () -> assertThat(response.getBody().getTimestamp()).isInstanceOf(LocalDateTime.class),
                    () -> assertThat(response.getBody().getErrors().get(0).getCode()).isEqualTo(GATEWAY_FAILED_CODE),
                    () -> assertThat(response.getBody().getErrors().get(0).getMessage()).isEqualTo(exception.getMessage())
            );
        }

    }

    @Nested
    @DisplayName("apiGatewayUnreachable Method")
    class APIGatewayUnreachableMethod {

        @Test
        @DisplayName("should returns a BAD GATEWAY response with body filled with APIErrorsResponse instance built from APIGatewayConnectionException exception")
        void returnsCustomResponseWhenAPIGatewayResponseExceptionIsThrown() {
            APIGatewayConnectionException exception = mock(APIGatewayConnectionException.class);
            when(exception.getMessage()).thenReturn("APIGatewayConnectionException Error Message");

            ResponseEntity<ApiErrorsResponse> response = exceptionHandlerAdvice.apiGatewayUnreachable(exception);

            assertAll("should return an instance of ResponseEntity with expected content",
                    () -> assertThat(response.getStatusCode()).isEqualTo(BAD_GATEWAY),
                    () -> assertThat(response.getBody().getStatus()).isEqualTo(BAD_GATEWAY.name()),
                    () -> assertThat(response.getBody().getCode()).isEqualTo(BAD_GATEWAY.value()),
                    () -> assertThat(response.getBody().getTimestamp()).isInstanceOf(LocalDateTime.class),
                    () -> assertThat(response.getBody().getErrors().get(0).getCode()).isEqualTo(UNREACHABLE_GATEWAY_CODE),
                    () -> assertThat(response.getBody().getErrors().get(0).getMessage()).isEqualTo(exception.getMessage())
            );
        }

    }

    @Nested
    @DisplayName("ConversionErrorHandler Method")
    class ConversionErrorHandlerMethod {

        @Test
        @DisplayName("should returns a BAD REQUEST response with body filled with APIErrorsResponse instance built from HttpMessageConversionException exception")
        void returnsCustomResponseWhenHttpMessageConversionExceptionIsThrown() {
            HttpMessageConversionException exception = mock(HttpMessageConversionException.class);
            when(exception.getMessage()).thenReturn("Conversion Error Message");

            ResponseEntity<ApiErrorsResponse> response = exceptionHandlerAdvice.conversionErrorHandler(exception);

            assertAll("should return an instance of ResponseEntity with expected content",
                    () -> assertThat(response.getStatusCode()).isEqualTo(BAD_REQUEST),
                    () -> assertThat(response.getBody().getStatus()).isEqualTo(BAD_REQUEST.name()),
                    () -> assertThat(response.getBody().getCode()).isEqualTo(BAD_REQUEST.value()),
                    () -> assertThat(response.getBody().getTimestamp()).isInstanceOf(LocalDateTime.class),
                    () -> assertThat(response.getBody().getErrors().get(0).getCode()).isEqualTo(REQUEST_FAILED_CODE),
                    () -> assertThat(response.getBody().getErrors().get(0).getMessage()).isEqualTo(exception.getMessage())
            );
        }

    }

    @Nested
    @DisplayName("genericErrorHandler Method")
    class GenericErrorHandlerMethod {

        @Test
        @DisplayName("should returns a INTERNAL SERVER ERROR response with body filled APIErrorsResponse instance built using static texts")
        void returnsCustomResponseWhenExceptionIsThrown() {
            ResponseEntity<ApiErrorsResponse> response = exceptionHandlerAdvice.genericErrorHandler(new Exception());

            assertAll("should return an instance of ResponseEntity with expected content",
                    () -> assertThat(response.getStatusCode()).isEqualTo(INTERNAL_SERVER_ERROR),
                    () -> assertThat(response.getBody().getStatus()).isEqualTo(INTERNAL_SERVER_ERROR.name()),
                    () -> assertThat(response.getBody().getCode()).isEqualTo(INTERNAL_SERVER_ERROR.value()),
                    () -> assertThat(response.getBody().getTimestamp()).isInstanceOf(LocalDateTime.class),
                    () -> assertThat(response.getBody().getErrors().get(0).getCode()).isEqualTo(UNEXPECTED_EXCEPTION_CODE),
                    () -> assertThat(response.getBody().getErrors().get(0).getMessage()).isEqualTo(UNEXPECTED_EXCEPTION_MESSAGE)
            );
        }

    }
}