package com.projecttemplate.api.apitemplate.presentation.validation.email;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.validation.ConstraintValidatorContext;

import static com.projecttemplate.api.apitemplate.utils.random.RandomAttributeUtils.randomEmail;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
@DisplayName("EmailFormatValidator Class")
class EmailFormatValidatorTest {

    @Mock
    private ConstraintValidatorContext constraintValidatorContext;

    private EmailFormatValidator emailFormatValidator;

    @BeforeEach
    void setUp() {
        emailFormatValidator = new EmailFormatValidator();
    }

    @Nested
    @DisplayName("isValid Method")
    class IsValidMethod {

        @Test
        @DisplayName("should returns true when email have all specifications")
        void isValidEmail() {
            String email = randomEmail();

            boolean valid = emailFormatValidator.isValid(email, constraintValidatorContext);

            assertThat(valid).isTrue();
        }

        @Test
        @DisplayName("should returns false when email doesn't contain the character @ (at)")
        void isInvalidEmailBecauseItDoesNotContainsTheAtCharacter() {
            String email = randomAlphanumeric(10) + "." + randomAlphanumeric(3);

            boolean valid = emailFormatValidator.isValid(email, constraintValidatorContext);

            assertThat(valid).isFalse();
        }

        @Test
        @DisplayName("should returns false when email doesn't contain any string before the character @ (at)")
        void isInvalidEmailBecauseItDoesNotContainsStringBeforeAtCharacter() {
            String email = "@" + randomAlphanumeric(10) + "." + randomAlphanumeric(3);

            boolean valid = emailFormatValidator.isValid(email, constraintValidatorContext);

            assertThat(valid).isFalse();
        }

        @Test
        @DisplayName("should returns false when email doesn't contain any string after the character @ (at) and before the character . (dot)")
        void isInvalidEmailBecauseItDoesNotContainsStringAfterAtCharacterAndBeforeDotCharacter() {
            String email = randomAlphanumeric(10) + "@." + randomAlphanumeric(3);

            boolean valid = emailFormatValidator.isValid(email, constraintValidatorContext);

            assertThat(valid).isFalse();
        }

        @Test
        @DisplayName("should returns false when email doesn't contain at least two letters after the character . (dot)")
        void isInvalidEmailBecauseItDoesNotContainsAtLeastTwoLetterAfterDotCharacter() {
            String email = randomAlphanumeric(10) + "@" + randomAlphanumeric(5) + "." + randomAlphanumeric(1);

            boolean valid = emailFormatValidator.isValid(email, constraintValidatorContext);

            assertThat(valid).isFalse();
        }

        @Test
        @DisplayName("should returns false when email is null")
        void isInvalidEmailBecauseItIsNull() {
            boolean valid = emailFormatValidator.isValid(null, constraintValidatorContext);

            assertThat(valid).isFalse();
        }

    }
}