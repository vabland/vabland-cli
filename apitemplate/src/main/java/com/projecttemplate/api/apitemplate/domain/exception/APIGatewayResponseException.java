package com.projecttemplate.api.apitemplate.domain.exception;

import org.springframework.http.HttpStatus;

public class APIGatewayResponseException extends RuntimeException {

    public APIGatewayResponseException(String url, HttpStatus httpStatus) {
        super("The service: " + url + " responded with https status: " + httpStatus.toString());
    }

}
