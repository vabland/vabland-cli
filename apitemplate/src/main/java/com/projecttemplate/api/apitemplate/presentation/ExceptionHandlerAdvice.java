package com.projecttemplate.api.apitemplate.presentation;

import com.projecttemplate.api.apitemplate.domain.exception.APIGatewayConnectionException;
import com.projecttemplate.api.apitemplate.domain.exception.APIGatewayResponseException;
import com.projecttemplate.api.apitemplate.presentation.response.ApiErrorResponse;
import com.projecttemplate.api.apitemplate.presentation.response.ApiErrorsResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Collections.singletonList;
import static org.springframework.http.HttpStatus.*;

@ControllerAdvice
class ExceptionHandlerAdvice {

    private static final String REQUEST_FAILED_CODE = "RequestFailed";
    private static final String GATEWAY_FAILED_CODE = "GatewayFailed";
    private static final String UNREACHABLE_GATEWAY_CODE = "UnreachableGateway";
    private static final String UNEXPECTED_EXCEPTION_CODE = "UnexpectedException";
    private static final String UNEXPECTED_EXCEPTION_MESSAGE = "Unexpected exception";

    @ExceptionHandler(MethodArgumentNotValidException.class)
    ResponseEntity<ApiErrorsResponse> validationErrorHandler(MethodArgumentNotValidException ex) {

        List<ApiErrorResponse> errorsResponses = createApiErrorResponses(ex);

        ApiErrorsResponse apiErrorsResponse = new ApiErrorsResponse(
                BAD_REQUEST.name(),
                BAD_REQUEST.value(),
                LocalDateTime.now(),
                errorsResponses
        );

        return ResponseEntity.status(BAD_REQUEST).body(apiErrorsResponse);
    }

    @ExceptionHandler(APIGatewayResponseException.class)
    ResponseEntity<ApiErrorsResponse> apiGatewayFailed(APIGatewayResponseException ex) {

        ApiErrorResponse error = new ApiErrorResponse(GATEWAY_FAILED_CODE, ex.getMessage());

        ApiErrorsResponse apiErrorsResponse = new ApiErrorsResponse(
                BAD_GATEWAY.name(),
                BAD_GATEWAY.value(),
                LocalDateTime.now(),
                singletonList(error)
        );

        return ResponseEntity.status(BAD_GATEWAY).body(apiErrorsResponse);
    }

    @ExceptionHandler(APIGatewayConnectionException.class)
    ResponseEntity<ApiErrorsResponse> apiGatewayUnreachable(APIGatewayConnectionException ex) {

        ApiErrorResponse error = new ApiErrorResponse(UNREACHABLE_GATEWAY_CODE, ex.getMessage());

        ApiErrorsResponse apiErrorsResponse = new ApiErrorsResponse(
                BAD_GATEWAY.name(),
                BAD_GATEWAY.value(),
                LocalDateTime.now(),
                singletonList(error)
        );

        return ResponseEntity.status(BAD_GATEWAY).body(apiErrorsResponse);
    }

    @ExceptionHandler(HttpMessageConversionException.class)
    ResponseEntity<ApiErrorsResponse> conversionErrorHandler(HttpMessageConversionException ex) {

        ApiErrorResponse error = new ApiErrorResponse(REQUEST_FAILED_CODE, ex.getMessage());

        ApiErrorsResponse apiErrorsResponse = new ApiErrorsResponse(
                BAD_REQUEST.name(),
                BAD_REQUEST.value(),
                LocalDateTime.now(),
                singletonList(error)
        );

        return ResponseEntity.status(BAD_REQUEST).body(apiErrorsResponse);
    }

    @ExceptionHandler(Exception.class)
    ResponseEntity<ApiErrorsResponse> genericErrorHandler(Exception ex) {

        ApiErrorResponse error = new ApiErrorResponse(UNEXPECTED_EXCEPTION_CODE, UNEXPECTED_EXCEPTION_MESSAGE);

        ApiErrorsResponse apiErrorsResponse = new ApiErrorsResponse(
                INTERNAL_SERVER_ERROR.name(),
                INTERNAL_SERVER_ERROR.value(),
                LocalDateTime.now(),
                singletonList(error)
        );

        return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(apiErrorsResponse);
    }

    private List<ApiErrorResponse> createApiErrorResponses(MethodArgumentNotValidException ex) {
        List<ApiErrorResponse> fieldErrors = extractFieldErrors(ex);
        List<ApiErrorResponse> generalErrors = extractGeneralErrors(ex);

        List<ApiErrorResponse> errorsResponses = new ArrayList<>();
        errorsResponses.addAll(fieldErrors);
        errorsResponses.addAll(generalErrors);
        return errorsResponses;
    }

    private List<ApiErrorResponse> extractFieldErrors(MethodArgumentNotValidException ex) {
        return ex.getBindingResult().getFieldErrors().stream().map(error ->
                new ApiErrorResponse(error.getField(), error.getCode(), error.getDefaultMessage())
        ).collect(Collectors.toList());
    }

    private List<ApiErrorResponse> extractGeneralErrors(MethodArgumentNotValidException ex) {
        return ex.getBindingResult().getGlobalErrors()
                .stream()
                .map(error -> {
                    String errorCode = getLastErrorCode(error);
                    return new ApiErrorResponse(errorCode, error.getDefaultMessage());
                }).collect(Collectors.toList());
    }

    private String getLastErrorCode(ObjectError error) {
        int lastPosition = error.getCodes().length - 1;
        return error.getCodes()[lastPosition];
    }
}
