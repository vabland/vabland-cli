package com.projecttemplate.api.apitemplate.presentation.validation.email;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({TYPE, FIELD, PARAMETER, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = EmailFormatValidator.class)
@Documented
public @interface ValidEmailFormat {

    String message() default "invalid email format";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
