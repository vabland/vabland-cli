package com.projecttemplate.api.apitemplate.application.utils;

import java.security.SecureRandom;
import java.util.Locale;
import java.util.Random;

public class Token {

    private static final String UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String LOWER = UPPER.toLowerCase(Locale.ROOT);
    private static final String DIGITS = "0123456789";
    private static final String ALPHANUMERIC = UPPER + LOWER + DIGITS;
    private static final int LENGTH = 100;
    private static final Random random = new SecureRandom();
    private static final char[] symbols = ALPHANUMERIC.toCharArray();

    public static String create() {
        char[] buf = new char[LENGTH];
        for (int idx = 0; idx < buf.length; ++idx)
            buf[idx] = symbols[random.nextInt(symbols.length)];
        return new String(buf);
    }

}
