package com.projecttemplate.api.apitemplate.presentation.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@AllArgsConstructor
public class ApiErrorsResponse {

    private String status;
    private Integer code;
    private LocalDateTime timestamp;
    private List<ApiErrorResponse> errors;

}
