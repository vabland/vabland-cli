package com.projecttemplate.api.apitemplate.infrastructure.gateway;

import com.projecttemplate.api.apitemplate.domain.exception.APIGatewayConnectionException;
import com.projecttemplate.api.apitemplate.domain.exception.APIGatewayResponseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;

@Component
public class APIGateway {

    private static final String APPLICATION_JSON_CHARSET_UTF_8 = "application/json; charset=utf-8";

    @Autowired
    private RestTemplate restTemplate;

    public APIGatewayRequestBuilder get(String host) {
        return new APIGatewayRequestBuilder(restTemplate, host, GET);
    }

    public APIGatewayRequestBuilder post(String host) {
        return new APIGatewayRequestBuilder(restTemplate, host, POST);
    }

    private HttpHeaders buildHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.set(CONTENT_TYPE, APPLICATION_JSON_CHARSET_UTF_8);
        return headers;
    }

    public class APIGatewayRequestBuilder {
        private UriComponentsBuilder uriComponentsBuilder;
        private Object requestBody;
        private RestTemplate restTemplate;
        private HttpMethod httpMethod;

        private APIGatewayRequestBuilder(RestTemplate restTemplate,
                                         String host,
                                         HttpMethod httpMethod) {
            this.restTemplate = restTemplate;
            this.uriComponentsBuilder = UriComponentsBuilder.fromHttpUrl(host);
            this.httpMethod = httpMethod;
        }

        public APIGatewayRequestBuilder paths(String... paths) {
            this.uriComponentsBuilder.pathSegment(paths);
            return this;
        }

        public APIGatewayRequestBuilder body(Object requestBody) {
            this.requestBody = requestBody;
            return this;
        }

        public APIGatewayRequestBuilder queryParam(MultiValueMap<String, String> queryParams) {
            this.uriComponentsBuilder.queryParams(queryParams);
            return this;
        }

        public <T> ResponseEntity<T> execute(Class<T> responseType) {
            HttpEntity<Object> httpEntity = buildHttpEntity();
            String url = uriComponentsBuilder.build().toString();
            try {
                return restTemplate.exchange(url, httpMethod, httpEntity, responseType);
            } catch (HttpStatusCodeException exception) {
                if (exception.getStatusCode().is4xxClientError()) {
                    return ResponseEntity.status(exception.getStatusCode()).build();
                }
                throw new APIGatewayResponseException(url, exception.getStatusCode());
            } catch (ResourceAccessException exception) {
                throw new APIGatewayConnectionException(url);
            }
        }

        private HttpEntity<Object> buildHttpEntity() {
            if (requestBody != null) {
                return new HttpEntity<>(requestBody, buildHeaders());
            }
            return new HttpEntity<>(buildHeaders());
        }
    }
}
