# Vabland CLI 

This code is responsible to create the base of any API for Vabland using the template
folder as source.

-----

### API Requirements

- Java 8
- Docker
- Lombok Plugin
- Enable Annotation Processing in Intellij Settings
- gcloud
- git

-----

### GCP Project and Authentication

- First, you need to have an account linked to gcp project with owner rights.
- Second, you need to enable Google Cloud Development to access your GCP Account Services through:
    - https://source.developers.google.com/new-password

-----

### API Creation

Execute the following command to create a new copy for a brand new API.

Description:
- _create_: the only operation available is create.
- _apiname_: should be the name of the API desired.
- _apiport_: should be four digits starting with number one. e.g.: 1000, 1001, 1002
- _dbport_: should be four digits starting with number two. e.g.: 2000, 2001, 2002


```bash
./cli
```

**Note 1: _By convention, Vabland API names should not contain the word API as prefix or suffix._**

**Note 2: _Check what ports other APIs are using and pick different ports to avoid conflict running locally._**

This command will create a folder for the new API.
Also will create a gradle module, a database and schema with the same name.

-----

### For windows users

It is advised to use Git Bash to execute bash scripts.
